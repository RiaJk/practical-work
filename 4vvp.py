def cg_distance(x1, y1, x2, y2, x3, y3, x4, y4) -> float:
    """
    Вычисляет расстояние между центрами
    тяжести двух прямоугольников
    parameters : координаты точек
    return: расстояние
    """
    # рассчитываем координаты центров тяжести
    mid_diagonal1_x = (x2 + x1) / 2
    mid_diagonal1_y = (y2 + y1) / 2
    mid_diagonal2_x = (x4 + x3) / 2
    mid_diagonal2_y = (y4 + y3) / 2
    # print(f'{mid_diagonal1_x = }\n{mid_diagonal1_y =}\n{mid_diagonal2_x =}\n{mid_diagonal2_y = }')
    # вычисляем расстояние между центрами
    distance = ((mid_diagonal2_x - mid_diagonal1_x)**2 + (mid_diagonal2_y - mid_diagonal1_y)**2)**(1/2)
    return round(distance, 2)


# print(cg_distance(1,2,3,0,3,4,5,2))


def corner_distance(x1, y1, x2, y2, x3, y3, x4, y4) -> float:
    """
    Вычисляет сумму расстояний между левыми верхними углами и правыми нижними
    parameters : координаты верхних левых и правых нижних углов
    return: расстояние
    """
    distance_top = ((x3 - x1)**2 + (y3 - y1)**2)**(1/2)
    distance_down = ((x4 - x2)**2 + (y4 - y2)**2)**(1/2)
    distance = distance_down + distance_top
    return round(distance, 2)


def input_coord() -> list[float]:
    '''
    Вводим координаты углов прямоугольников
    return:
    '''
    while True:
        try:
            coordinates = list(map(float, input('coordinates').split()))
        except ValueError:
            print("STUPID")
            continue
        if len(coordinates) == 8:
            break
    return coordinates


def main():
    while True:
        info()
        command = input('command: ')
        if command == 'center_gr':
            coordinates = input_coord()
            print(cg_distance(*coordinates))
        elif command == 'corner':
            coordinates = input_coord()
            print(corner_distance(*coordinates))
        elif command == 'stop':
            return
        else:
            print('stupid')


def info():
    """
    Выводит инструкцию к программе
    return: None
    """
    print("введите чтобы вычислить:\n"
          "'center_gr' - расстояние между центрами\n"
          "'corner' - сумму расстояний между углами\n"
          "или\n"
          "'stop' - чтобы завершить программу\n")


if __name__ == '__main__':
    print(main())

# Привет, Вика
# Что делаешь



